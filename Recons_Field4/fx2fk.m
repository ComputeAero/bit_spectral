function y=fx2fk(fx,km,x,Nx)
fk=0;
for q=1:1:Nx
   fk=fk+fx(q)*exp(-i*km*x(q)); 
end
fk=fk/Nx;
y=fk;