function y=timeadvance(f,dt,a,N_x,dx)
 
f0=f;
for m=1:1:3
    
    switch m
        
    case 1
        f=f0;
    case 2
        f=f1;
    case 3
        f=f2;  
    end
               
    df=Diff_OMUSCL2(f,N_x,dx); 
   
    rf=-a*df;       
            
    switch m
    case 1
        f1=f+dt*rf; 
    case 2
        f2=(3/4)*f0+(1/4)*f1+(1/4)*dt*rf;    
    case 3
        f3= (1/3)*f0+(2/3)*f2+(2/3)*dt*rf;  
        f=f3;
    end 
            
end
      
y=f;


