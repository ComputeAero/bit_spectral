%----------------Nonlinear Spectral Analysis of WENO-JS-----------
clear all;clc
%-----------------------------
global kexi yita Flag_limiter a1 a2 a3 a4

for Flag_ky      = [6:7]     % 1- CD4;  2- Li's paper; 
for Flag_limiter = [0]
    
Total        = 1e4;         %number of total tests

if     Flag_ky == 1   
    kexi = -1/3; 
    yita =    0; 
elseif Flag_ky == 2
    kexi = -1/3;
    yita =  0.2;
elseif Flag_ky == 3
    kexi = -1/3;
    yita =  0.5;   
elseif Flag_ky == 4
    kexi = -0.55;
    yita =     0; 
elseif Flag_ky == 5
    kexi = -0.55;
    yita =   0.2; 
elseif Flag_ky == 6
    kexi = -0.55;
    yita =   0.5;
elseif Flag_ky == 7
    kexi =    -1;
    yita =     0;
elseif Flag_ky == 8
    kexi =    -1;
    yita =   0.2;
elseif Flag_ky == 9
    kexi =    -1;
    yita =   0.5;
      
else
    disp('Flag_ky is error!!!');
    return;
end

% -------- After setting -----------

a1 = ( kexi -   yita    )/4;
a2 = (-kexi + 3*yita + 2)/4;
a3 = (-kexi - 3*yita + 2)/4;
a4 = ( kexi +   yita    )/4;

color_av='r-';
color_sigma='r.';


%---------------------------------
Nx = 512;  dx=2*pi/Nx;   xmax=2*pi-dx;   x=0:dx:xmax;


%-----------------------------
k_max = Nx/2;   k = -k_max:1:k_max; redu_wavenum=k*dx;
Nk=size(k,2);   k_ratio=1/2;    kc=floor(k_max*k_ratio);
%-----------------------------
  u0      = 1;
  alpha   = -5/3;
  
  %-----allocate variables----------

  fei_save=zeros(Nk,Total);
  E_dudx=zeros(Nk,Total);
  %--------------------------------------

load fei.mat;
%---------start realization----------------------
for testindex=1:1:Total
    
    if mod(testindex, 100) == 0 
        disp(['The ', num2str(testindex), ' Step.']);
    end
    
    
      %------generate random phase----------
   fei=fei_save(:,testindex)';
    
    uk=zeros(1,Nk);
   %----------case 1: original distribution------------
    for n=1:1:Nk
        if k(n)<=kc && k(n)>=1
              uk(n) = sqrt(u0)*((1+1i*tan(fei(n)))./(sqrt(1+(tan(fei(n))).^2))).*(abs(k(n))).^(alpha/2);
        else
             uk(n) =0;  
        end
    end
    temp=uk;
    uk(Nx/2:-1:1)=conj(temp(Nx/2+2:end));
 
    %----------case 1: original distribution  END---------------
  %-------plots of {uk}------------      
% figure(3)
% plot(k,real(uk))
% figure(4)
% plot(k,imag(uk))
% figure(5)
% plot(k,abs(uk))  
%-----------------------------------------
   Euk=(abs(uk)).^2;   % Energy, used in later research
%    figure(6)
%    loglog(k/k_max,Euk./(k.^2),'ks')  ;
%--------project {uk} to physical space {ux} using Inversed FT------
    ux=zeros(1,Nx);
    for n=1:1:Nx
        ux(n) = 0;
        for q=-k_max:1:k_max       % set the wavenumber considered
            qq=q+k_max+1;  
            ux(n)=ux(n)+uk(qq).*exp(1i*x(n)*q);
        end
        ux(n) = ux(n)/Nk;
    end
    
    ux=real(ux);                    %obtain the real part of ux
%----calculate the derivative--------------------    
   
        dudx = Diff_OMUSCL2 (ux,Nx,dx);

    
%-------FT to dudx-----------------
 F_dudx=zeros(1,Nk);
    for q=-k_max:1:k_max
        qq=q+k_max+1;
        kn=k(qq);
        F_dudx(n)=0;
        for p=1:1:Nx
   			F_dudx(qq)= F_dudx(qq)+dudx(p).*exp(-1i*kn*x(p)); 
        end
    end
%---------Energy---------------------

E_dudx(:,testindex)=(abs(F_dudx).^2)';

% disp(['The ',num2str(testindex),' is completed']);
end   %  end of the testindex
  
  %---------calculate average E_dudx----------
  E_dudx_av = mean(E_dudx,2);
  %------calculate the variation----------------
  E_dudx_sigma=sigma(E_dudx,E_dudx_av,Total);
  E_dudx_s1=E_dudx_av+2*E_dudx_sigma;
  E_dudx_s2=E_dudx_av-2*E_dudx_sigma;

  %--------extract the part with positive wavenumber for visualization------
  k=k(Nx/2+1:end);
  E_dudx_av=E_dudx_av(Nx/2+1:end)';E_dudx_s1=E_dudx_s1(Nx/2+1:end)';E_dudx_s2=E_dudx_s2(Nx/2+1:end)';
  Euk=Euk(Nx/2+1:end)';
  
  
filename = Name_Gen('Recons',kexi, yita, Flag_limiter);

save(filename);
  
  
  
%----------Visualize----------- 
figure(13)
loglog(k/k_max,E_dudx_av./(k.^2),color_av,'linewidth',2);hold on;
loglog(k/k_max,E_dudx_s1./(k.^2),color_sigma,k/k_max,E_dudx_s2./(k.^2),color_sigma,'linewidth',2);
loglog(k/k_max,Euk,'k-');hold on;

close all;

end
end

