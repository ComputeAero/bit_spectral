function y=ADRmethod(Nx,x,dx,k)


sk=size(k,2);   
ki=zeros(sk,1);
kr=zeros(sk,1);

for m=1:1:sk
    
    km=k(m);

    angle=0; u0=1;
    f=u0*exp(1i*km*x).*exp(1i*angle);  
    f=real(f);
    
    df=Diff_OMUSCL2(f,Nx,dx);  
   
    fk=2*fx2fk(df,km,x,Nx)/u0;
    
    
    k_mod=fk*dx;
    kr(m)=real(k_mod);
    ki(m)=imag(k_mod);
end

imfei=-kr;
refei=ki;

%-----eliminate the oscilation--------

Nk=Nx/2;
imfei(Nk/2)=(imfei(Nk/2+1)+imfei(Nk/2-1))/2;
imfei(Nk*3/4)=(imfei(Nk*3/4+1)+imfei(Nk*3/4-1))/2;
imfei(Nk*7/8)=(imfei(Nk*7/8+1)+imfei(Nk*7/8-1))/2;
imfei(end)=imfei(end-1);

refei(Nk/2)=(refei(Nk/2+1)+refei(Nk/2-1))/2;
refei(Nk*3/4)=(refei(Nk*3/4+1)+refei(Nk*3/4-1))/2;
refei(Nk*7/8)=(refei(Nk*7/8+1)+refei(Nk*7/8-1))/2;

%---------------------------
%------return the results
y(:,1)=k*dx;
y(:,2)=imfei;
y(:,3)=refei;