function filename = Name_Gen (section,kexi, yita, Flag_limiter)

kexi = num2str(kexi);
yita = num2str(yita);

if kexi(1) == '-'
    sign_kexi = 'N';
    kexiname  = strcat(kexi(2),kexi(4:end));
else
    sign_kexi = 'P';
    kexiname  = strcat(kexi(1),kexi(3:end));  
end

if size(kexiname,2) > 3
    kexiname = kexiname(1:3);
end


if yita(1) == '-'
    sign_yita = 'N';
    yitaname  = strcat(yita(2),yita(4:end)); 
else
    sign_yita = 'P';
    yitaname  = strcat(yita(1),yita(3:end)); 
end

if size(yitaname,2) > 3
    yitaname = yitaname(1:3);
end

Flag_limiter = num2str(Flag_limiter);

filename = (strcat(section, '_', 'Limit_',Flag_limiter,...
    '_','kexi_',sign_kexi,kexiname, '_','yita_',sign_yita,yitaname,'.mat'));



