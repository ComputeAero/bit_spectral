%------Spectral analysis using ADR---------------
clear all;clc

global kexi yita Flag_limiter a1 a2 a3 a4

for Flag_ky      = [2:3]    % 1- CD4;  2- Li's paper; 
for Flag_limiter = [0]

Nx           = 512;

if     Flag_ky == 1   
    kexi = -1/3; 
    yita =    0; 
elseif Flag_ky == 2
    kexi = -1/3;
    yita =  0.2;
elseif Flag_ky == 3
    kexi = -1/3;
    yita =  0.5;   
elseif Flag_ky == 4
    kexi = -0.55;
    yita =     0; 
elseif Flag_ky == 5
    kexi = -0.55;
    yita =   0.2; 
elseif Flag_ky == 6
    kexi = -0.55;
    yita =   0.5;
elseif Flag_ky == 7
    kexi =    -1;
    yita =     0;
elseif Flag_ky == 8
    kexi =    -1;
    yita =   0.2;
elseif Flag_ky == 9
    kexi =    -1;
    yita =   0.5;
      
else
    disp('Flag_ky is error!!!');
    return;
end

% -------- After setting -----------

a1 = ( kexi -   yita    )/4;
a2 = (-kexi + 3*yita + 2)/4;
a3 = (-kexi - 3*yita + 2)/4;
a4 = ( kexi +   yita    )/4;

dx=2*pi/Nx;
xmax=2*pi-dx;   
x=0:dx:xmax;

Nk=Nx/2;     
k=1:1:Nk;  % set the wavenumber interested

y=ADRmethod(Nx,x,dx,k);

imfei= y(:,2);
refei= y(:,3);
fei  = y(:,1);

refei_exact = fei;
imfei_exact = zeros(size(k,2),1);


%-------- save process --------

filename = Name_Gen('./Data/ADR/ADR',kexi, yita, Flag_limiter);

save(filename,'fei','refei_exact','refei','imfei_exact','imfei');

%---------plot figures ---------

figure(1)
plot(fei,refei_exact,'-k',fei,refei,'r-o','linewidth',2);hold on
pause(1)

figure(2)
plot(fei,imfei_exact,'-k',fei,imfei,'b-s','linewidth',2);hold on;
pause(1)

% close all;

end
end