clear all; clc;

Nx = 512;
Nk = Nx + 1;

Total = 1e4;

fei_save = pi * (2*rand(Nk, Total) - 1);

save('../ADR/fei.mat','fei_save');
save('../NSA/fei.mat','fei_save');
save('../Recons_Field/fei.mat','fei_save');
save('../Energy_Lost/fei.mat','fei_save');
save('../Discontinous/fei.mat','fei_save');



