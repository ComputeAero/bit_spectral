%---------------------------------
% NSA for OMUSCL2
%---------------------------------

clear all;clc
%-----------------------------
global kexi yita Flag_limiter a1 a2 a3 a4

for Flag_ky      = [1:5]    % 1- CD4;  2- Li's paper; 
for Flag_limiter = [1]


Total        = 1e4;         %number of total tests

if     Flag_ky == 1   
    kexi = -1/3; 
    yita =    0; 
elseif Flag_ky == 2
    kexi = -1/3;
    yita =  0.2;
elseif Flag_ky == 3
    kexi = -1/3;
    yita =  0.5;   
elseif Flag_ky == 4
    kexi = -0.55;
    yita =     0; 
elseif Flag_ky == 5
    kexi = -0.55;
    yita =   0.2; 
elseif Flag_ky == 6
    kexi = -0.55;
    yita =   0.5;
elseif Flag_ky == 7
    kexi =    -1;
    yita =     0;
elseif Flag_ky == 8
    kexi =    -1;
    yita =   0.2;
elseif Flag_ky == 9
    kexi =    -1;
    yita =   0.5;
      
else
    disp('Flag_ky is error!!!');
    return;
end

% -------- After setting -----------

a1 = ( kexi -   yita    )/4;
a2 = (-kexi + 3*yita + 2)/4;
a3 = (-kexi - 3*yita + 2)/4;
a4 = ( kexi +   yita    )/4;


color_av    = 'r-';


color_sigma = 'r.';


%---------------------------------
Nx = 512;  dx=2*pi/Nx;   xmax=2*pi-dx;   x=0:dx:xmax;

%-----------------------------
k_max = Nx/2;   k = -k_max:1:k_max; redu_wavenum=k*dx;
Nk=size(k,2);
%-----------------------------
  u0      = 1;
  alpha   = -5/3;
  k_ratio = 1;
  
  %-----allocate variables----------
  Re_f = zeros(Nk,Total);
  Im_f = zeros(Nk,Total);

load fei.mat
%---------------------------------------------
for testindex=1:1:Total
    if mod(testindex, 100) == 0 
        disp(['The ', num2str(testindex), ' Step.']);
    end
  %------random phase----------
    fei=fei_save(:,testindex)';  % use the same random phase of WENO-JS
    
    uk=zeros(1,Nk);
   %----------case 1: original distribution------------
    for n=1:1:Nk
        if k(n)>=1
              uk(n) = sqrt(u0)*((1+1i*tan(fei(n)))./(sqrt(1+(tan(fei(n))).^2))).*(abs(k(n))).^(alpha/2);
        else
             uk(n) =0;  
        end
    end
    temp=uk;
    uk(Nx/2:-1:1)=conj(temp(Nx/2+2:end));
 
    %----------case 1: original distribution  END---------------
  %-------plots of {uk}------------      
% figure(3)
% plot(k,real(uk))
% figure(4)
% plot(k,imag(uk))
% figure(5)
% plot(k,abs(uk))  
%-----------------------------------------
    Euk=(abs(uk)).^2;   % Energy, used in later research
    
%--------project {uk} to physical space {ux} using Inversed FT------
    ux=zeros(1,Nx);
    for n=1:1:Nx
        ux(n) = 0;
        for q=-k_max:1:k_max       % set the wavenumber considered
            qq=q+k_max+1;  
            ux(n)=ux(n)+uk(qq).*exp(1i*x(n)*q);
        end
        ux(n) = ux(n)/Nk;
    end
    
    ux=real(ux);                    %obtain the real part of ux
%     plot(x,ux)

 
    dudx = Diff_OMUSCL2(ux,Nx,dx);     

%-------FT to dudx-----------------
 F_dudx=zeros(1,Nk);
    for q=-k_max:1:k_max
        qq=q+k_max+1;
        kn=k(qq);
        F_dudx(n)=0;
        for p=1:1:Nx
   			F_dudx(qq)= F_dudx(qq)+dudx(p).*exp(-1i*kn*x(p)); 
        end
    end
%---------modified wavenumber---------------------
     Modi = F_dudx./(1i*uk);

     Re_f(:,testindex) =  real(Modi)';
     Im_f(:,testindex) =  imag(Modi)';
%     disp(['The ',num2str(testindex),' is completed']);
end
  
  %---------calculate average-----------
  Re_f_av = mean(Re_f,2);
  Im_f_av = mean(Im_f,2);
  
  Re_error_k_av=(real(k)'-Re_f_av);
  Im_error_k_av=(imag(k)'-Im_f_av);
  
  %------calculate the variation----------------
  Re_f_sigma=sigma(Re_f,Re_f_av,Total);
  Re_f_s1=Re_f_av+2*Re_f_sigma;
  Re_f_s2=Re_f_av-2*Re_f_sigma;
  Re_error_k_s1=(real(k)'-Re_f_s1);
  Re_error_k_s2=(real(k)'-Re_f_s2);
    %---------------
  Im_f_sigma=sigma(Im_f,Im_f_av,Total);
  Im_f_s1=Im_f_av+2*Im_f_sigma;
  Im_f_s2=Im_f_av-2*Im_f_sigma;
  Im_error_k_s1=(imag(k)'-Im_f_s1);
  Im_error_k_s2=(imag(k)'-Im_f_s2);
    %----------------------
  
  %--------extract the part with positive wavenumber for visualization------
  k=k(Nx/2+1:end);
  Re_f_av=Re_f_av(Nx/2+1:end)';Re_f_s1=Re_f_s1(Nx/2+1:end)';Re_f_s2=Re_f_s2(Nx/2+1:end)';
  Im_f_av=Im_f_av(Nx/2+1:end)';Im_f_s1=Im_f_s1(Nx/2+1:end)';Im_f_s2=Im_f_s2(Nx/2+1:end)';
  
  Re_error_k_av=Re_error_k_av(Nx/2+1:end)';Re_error_k_s1=Re_error_k_s1(Nx/2+1:end)';Re_error_k_s2=Re_error_k_s2(Nx/2+1:end)';
  Im_error_k_av=Im_error_k_av(Nx/2+1:end)';Im_error_k_s1=Im_error_k_s1(Nx/2+1:end)';Im_error_k_s2=Im_error_k_s2(Nx/2+1:end)';
  
  Re_f_sigma=Re_f_sigma(Nx/2+1:end)';  Im_f_sigma=Im_f_sigma(Nx/2+1:end)';

  
filename = Name_Gen('NSA',kexi, yita, Flag_limiter);

save(filename);
  
 
%----------Visualize----------- 
figure(1)
plot(k/k_max,k/k_max,'-k',k/k_max,Re_f_av/k_max,color_av,'linewidth',2);hold on;
plot(k/k_max,Re_f_s1/k_max,color_sigma,k/k_max,Re_f_s2/k_max,color_sigma,'linewidth',2);

figure(2)
plot(k/k_max,zeros(1,Nx/2+1),'-k',k/k_max,Im_f_av/k_max,color_av,'linewidth',2);hold on;
plot(k/k_max,Im_f_s1/k_max,color_sigma,k/k_max,Im_f_s2/k_max,color_sigma,'linewidth',2);

close all;
end
end

