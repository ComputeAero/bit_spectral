%--------Visualize function--------------
clear all;clc
close all;

  load data_uw5.mat
  figure(88)
  plot(k,abs(uk_0),'ko-');hold on
  plot(k,abs(uk_tmax),'m^-');hold on
  
  load data_js.mat
  plot(k,abs(uk_tmax),'rs-');hold on

  load data_z.mat
  plot(k,abs(uk_tmax),'bo-');hold on
  
  
  legend('t=0','t=tmax(UW5)','t=tmax(WENO-JS5)','t=tmax(WENO-Z5)');