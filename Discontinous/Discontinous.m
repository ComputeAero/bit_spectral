%----------------Nonlinear Spectral Analysis of WENO-JS-----------
clear all;clc
%-----------------------------

global kexi yita Flag_limiter a1 a2 a3 a4

for Flag_ky      = [1:9]     % 1- CD4;  2- Li's paper; 
for Flag_limiter = [0, 1]

dt           = 1e-4; 

if     Flag_ky == 1   
    kexi = -1/3; 
    yita =    0; 
elseif Flag_ky == 2
    kexi = -1/3;
    yita =  0.2;
elseif Flag_ky == 3
    kexi = -1/3;
    yita =  0.5;   
elseif Flag_ky == 4
    kexi = -0.55;
    yita =     0; 
elseif Flag_ky == 5
    kexi = -0.55;
    yita =   0.2; 
elseif Flag_ky == 6
    kexi = -0.55;
    yita =   0.5;
elseif Flag_ky == 7
    kexi =    -1;
    yita =     0;
elseif Flag_ky == 8
    kexi =    -1;
    yita =   0.2;
elseif Flag_ky == 9
    kexi =    -1;
    yita =   0.5;
      
else
    disp('Flag_ky is error!!!');
    return;
end

% -------- After setting -----------

a1 = ( kexi -   yita    )/4;
a2 = (-kexi + 3*yita + 2)/4;
a3 = (-kexi - 3*yita + 2)/4;
a4 = ( kexi +   yita    )/4;


color_av='r-';
color_sigma='r.';
Total =1;         %number of total tests

%--------select numerical scheme------------
% diff_flag = flag_WENO5;  
%---------------------------------
Nx = 512;  dx=2*pi/Nx;   xmax=2*pi-dx;   x=0:dx:xmax;
%-----------------------------
k_max = Nx/2;   k = -k_max:1:k_max; redu_wavenum=k*dx;  
Nk=size(k,2);    k_ratio=2/3;    kc=floor(k_max*k_ratio);
%-----set advective velocity-----
a=pi;
%-------time evolution---------
tmax=2;         t=0:dt:tmax;

%-----------------------------
  u0      = 1;
  alpha   = -5/3;

  
  %-----allocate variables----------
  Re_f = zeros(Nk,Total);
  Im_f = zeros(Nk,Total);
  fei_save=zeros(Nk,Total);
  dE_save=zeros(Nk,Total);
  Ed_save=zeros(Nk,Total);
  
%---------start realization----------------------

  for testindex=1:1:Total
      %------generate random phase----------

   %-------construct the new u(x)--------------
    Nx1 = floor(Nx*1/3);       Nx2 = floor(Nx*2/3);     Shock=1;
    ux_shock=zeros(1,Nx);
    ux_shock(Nx1:1:Nx2)=Shock;
    ux=ux_shock;
   %-----------------------------
  
  %---------------calculate the initial uk_0-----------
  uk_0=zeros(1,Nk);
  for n=1:1:Nk
    kn=k(n);
    fk=fx2fk(ux,kn,x,Nx);
    fk=fk*Nx;
    uk_0(n)=fk;
%     disp([num2str(kn),'  is completed (synthetic wave)']); 
  end

%   figure(7)
%   plot(k,real(uk),'ks',k,real(uk_0),'r-')
%   figure(8)
%   plot(k,imag(uk),'ks',k,imag(uk_0),'r-')
  %---------computation for scheme------------
  [uk_tmax,ux_tmax]=Timead_spec_syn_t(dt,ux, Nx,Nk,a,x,dx,t,k);

  %--------------error of Energy 1------
  dE=abs(((uk_0.*conj(uk_0))-(uk_tmax.*conj(uk_tmax)))/(sum(uk_0.*conj(uk_0))));
  %--------------error of Energy 2-------------
  Ed=abs(((uk_0-uk_tmax).*conj((uk_0-uk_tmax)))/(sum(uk_0.*conj(uk_0))));
  
  dE_save(:,testindex)=dE';
  Ed_save(:,testindex)=Ed';
  
  end
  
  
dE_tmax=mean(dE_save,2)';
Ed_tmax=mean(Ed_save,2)';
  

filename = Name_Gen('Discont',kexi, yita, Flag_limiter);

save(filename);

  
 close all;
 
end
end
  
  
  
  
  
  
  
  