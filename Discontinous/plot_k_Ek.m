%--------Visualize function--------------
clear all;clc
close all;

  
load data_js.mat

Ek_tmax=abs(uk_tmax).^2;
index=((Nk-1)/2)+2:1:Nk;
k=k(index);    Ek_tmax=Ek_tmax(index);
% k=k/k_max;
loglog(k/k_max,Ek_tmax,'rs-');hold on

  
axis([0 1 1e-10 max(Ek_tmax)]);
