%--------Visualize function--------------
%Notes: 计算WENO与UW5谱能量分布的差值
%-----------------------------
clear all;clc
close all;
 %-----------UW5--------------------------------------
  clear all;
  load data_uw5.mat
  k_max=max(k);  
  k_max=1;
  Nk=size(k,2);
  Ek_0=abs(uk_0).^2;
  Ek_tmax=abs(uk_tmax).^2;
  index=((Nk-1)/2)+2:1:Nk;
  k=k(index);
  Ek_0=Ek_0(index);
  Ek_tmax=Ek_tmax(index);
  EUW5=Ek_tmax;
%   Ek_90=Ek_tmax(90),
  figure(88)
  loglog(k/k_max,Ek_0,'ro-');hold on
  loglog(k/k_max,Ek_tmax,'b-');hold on
  legend('t=0','t=tmax');
  
  
  %--------WENO-JS---------------
load data_js.mat
k_max=1;
 Nk=size(k,2);
  Ek_0=abs(uk_0).^2;
  Ek_tmax=abs(uk_tmax).^2;
  index=((Nk-1)/2)+2:1:Nk;
  k=k(index);
  Ek_0=Ek_0(index);
  Ek_tmax=Ek_tmax(index);
%   Ek_90=Ek_tmax(90),
figure(99)
loglog(k/k_max,Ek_0,'ro-');hold on
  loglog(k/k_max,Ek_tmax,'b-');hold on
  legend('t=0','t=tmax');
  
  
  %-----------WENO-Z--------------------------------------
  
  load data_z.mat
  k_max=1;
  figure(100)
  Nk=size(k,2);
  Ek_0=abs(uk_0).^2;
  Ek_tmax=abs(uk_tmax).^2;
  index=((Nk-1)/2)+2:1:Nk;
  k=k(index);
  Ek_0=Ek_0(index);
  Ek_tmax=Ek_tmax(index);
%   Ek_90=Ek_tmax(90),
  loglog(k/k_max,Ek_0,'ro-');hold on
  loglog(k/k_max,Ek_tmax,'b-');hold on
legend('t=0','t=tmax');

%------------------------------------
% figure(88)
%   legend('UW5','WENO-JS5','WENO-Z5',2);