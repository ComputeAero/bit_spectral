%--------Visualize function--------------
clear all;clc
close all;
 %-----------UW5--------------------------------------
  clear all;clc
  load data_uw5.mat
  figure(12)
  loglog(k/k_max,dE_tmax,color_av,'linewidth',2,'Markersize',4);hold on
  figure(13)
  loglog(k/k_max,Ed_tmax,color_av,'linewidth',2,'Markersize',4);hold on
%--------WENO-JS---------------
load data_js.mat
figure(12)
loglog(k/k_max,dE_tmax,color_av,'linewidth',2,'Markersize',4);hold on
figure(13)
loglog(k/k_max,Ed_tmax,color_av,'linewidth',2,'Markersize',4);hold on

  %-----------WENO-Z--------------------------------------
  clear all;clc
  load data_z.mat
  figure(12)
  loglog(k/k_max,dE_tmax,'-b','linewidth',2,'Markersize',4);hold on
  figure(13)
  loglog(k/k_max,Ed_tmax,'-b','linewidth',2,'Markersize',4);hold on



%------------------------------------
figure(12)
  title('{\Delta}E_u'); xlabel('k/k_{max}');ylabel('{\Delta}E_u(t/t_{end}=1)');
  axis([2e-3 1 1e-13 1e-3]);
  legend('UW5','WENO-JS5','WENO-Z5',3);
  figure(13)
  title('E_{{\Delta}u}'); xlabel('k/k_{max}');ylabel('E_{{\Delta}u}(t/t_{end}=1)');
  axis([2e-3 1 1e-13 1e-1]);
  legend('UW5','WENO-JS5','WENO-Z5',2);