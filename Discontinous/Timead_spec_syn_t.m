function [uk_tmax,ux_tmax]=Timead_spec_syn_t(dt,wave,Nx,Nk,a,x,dx,t,k)
%-------------------------------
uk_temp=zeros(1,Nk);
t_size=size(t,2);
f=wave;
%--------time evolution----------
for ll=1:1:t_size  
     f=timeadvance(f,dt,a,Nx,dx);  
                %--display the time ----
              if mod(ll-1,100)==0
              disp(num2str(t(ll)));  
              end
              
%  figure(1); cla; plot(x,f,'ro-');           
              
              
end
ux_tmax=f;
%------amplitude obtained by Fourier transformation-------------
for n=1:1:Nk
    kn=k(n);
    fk=fx2fk(f,kn,x,Nx);
    fk=fk*Nx;
    uk_temp(n)=fk;
%     disp([num2str(kn),'  is completed (synthetic wave)']); 
end
uk_tmax=uk_temp;