function df = Diff_OMUSCL2(f, N, dx)

global kexi yita Flag_limiter a1 a2 a3 a4

f_half=zeros(N+1,1);

LB = [f(N-1), f(N)];
RB = [f(  1), f(2)];
f  = [LB, f, RB];   % 1, 2, .... N+4

for j = 1 : 1 : N+1  %3...98
    
    if Flag_limiter == 0
    
        f_half(j) = a1 * f(j) + a2 * f(j+1) + a3 * f(j+2) + a4 * f(j+3);
 
    elseif Flag_limiter == 1
        
        R1 = (f(j+1) - f(j  ))/(f(j+2) - f(j+1));
        R2 = (f(j+3) - f(j+2))/(f(j+2) - f(j+1));

        fei = (1 - yita) + 0.5*(yita+kexi) * R2 + 0.5*(yita-kexi) * R1;

        fei = max([0, min([2, fei, 2*R1])]);

        f_half(j) = f(j+1) + 0.5 * fei * (f(j+2) - f(j+1));
    
    end
    
end

df=zeros(1,N);

for j = 1 : 1 : N
    
	df(j) = (f_half(j+1) - f_half(j))/dx;
    
end
               


