function sigma_x_av=sigma(x,x_av,N)

error=zeros(size(x));

for j=1:1:N
    error(:,j) = x(:,j)-x_av;
end

sigma_x_av=sqrt(((sum(error.^2,2))/(N-1)));